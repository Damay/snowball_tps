// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/SnowBall_TPSCharHealthComponent.h"

void USnowBall_TPSCharHealthComponent::ChangeHealthValue(float ChangeValue)
{
	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
		if (Shield <= 0)
		{
			//ToDo
			UE_LOG(LogTemp, Warning, TEXT("USnowBall_TPSCharHealthComponent::ChangeHealthValue - Shield <= 0"));
		}
	}
	else
	{
		Super::ChangeHealthValue(ChangeValue);
	}
}

float USnowBall_TPSCharHealthComponent::GetCurrentShield()
{
	return Shield;
}

void USnowBall_TPSCharHealthComponent::ChangeShieldValue(float ChangeValue)
{

		Shield += ChangeValue * CoefDamage;
		OnShieldChange.Broadcast(Shield, ChangeValue);

	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
		{
			Shield = 0.0f;
		}
	}

	if (GetWorld() && Shield <= 100.0f)
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ColldownShield, this, &USnowBall_TPSCharHealthComponent::CollDownShieldEnd, CollDownShieldRecoveryTime, false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}
}

void USnowBall_TPSCharHealthComponent::CollDownShieldEnd()
{
	if (GetWorld() && Live == true)
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ColldownShield, this, &USnowBall_TPSCharHealthComponent::RecoveryShield, ShieldRecoveryRate, true);
	}
}

void USnowBall_TPSCharHealthComponent::RecoveryShield()
{
	float NewShieldValue = Shield + ShieldRecoveryValue;

	if (NewShieldValue >= 100.0f)
	{
		Shield = 100.0f;

		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = NewShieldValue;
	}
	OnShieldChange.Broadcast(Shield, ShieldRecoveryValue);
}
