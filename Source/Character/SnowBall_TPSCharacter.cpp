// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnowBall_TPSCharacter.h"
//#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
//#include "HeadMountedDisplayFunctionLibrary.h"
//#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
//#include "Engine/DataTable.h"
#include "Game/SnowBall_TPSGameInstance.h"
#include "Math/UnrealMathUtility.h"
#include "Components/SceneComponent.h"
#include "Animation/AnimSequenceBase.h"
#include "Components/StaticMeshComponent.h"
#include "Weapon/Projectiles/ProjectileDefault.h"
#include "Engine/World.h"

ASnowBall_TPSCharacter::ASnowBall_TPSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<USnowBall_TPSInventoryComponent>(TEXT("InventoryComponent"));


	if (InventoryComponent)
	{
		InventoryComponent->OnSwichWeapon.AddDynamic(this, &ASnowBall_TPSCharacter::InitWeapon);
	}

	CharHealthComponent = CreateDefaultSubobject<USnowBall_TPSCharHealthComponent>(TEXT("CharHealthComponent"));
	if (CharHealthComponent)
	{
		CharHealthComponent->OnDead.AddDynamic(this, &ASnowBall_TPSCharacter::CharDead);
	}
	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ASnowBall_TPSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}
	MovementTick(DeltaSeconds);
}

void ASnowBall_TPSCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

void ASnowBall_TPSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ASnowBall_TPSCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ASnowBall_TPSCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ASnowBall_TPSCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ASnowBall_TPSCharacter::InputAttackReleased);

	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ASnowBall_TPSCharacter::TryReloadWeapon);

	NewInputComponent->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Released, this, &ASnowBall_TPSCharacter::TryAbilityEnabled);
	
	NewInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &ASnowBall_TPSCharacter::TrySwitchWeapon);
}

UDecalComponent* ASnowBall_TPSCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

void ASnowBall_TPSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ASnowBall_TPSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ASnowBall_TPSCharacter::InputAttackPressed()
{
	if (bIsAllive)
	{
		AttackCharEvent(true);
	}
}

void ASnowBall_TPSCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ASnowBall_TPSCharacter::TryReloadWeapon()
{
	if (bIsAllive && CurrentWeapon && CurrentWeapon->WeaponReloading == false)
	{
		if ((CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound) && CurrentWeapon->CheckWeaponReload())
		{
			CurrentWeapon->StartReload();
		}
	}
}

void ASnowBall_TPSCharacter::TrySwitchWeapon()
{
	if (InventoryComponent->WeaponSlot.Num() > 1)
	{
		const int8 OldIndex = CurrentIndexWeapon;

		if (CurrentWeapon)
		{
			const FAdditionalWeaponInfo OldInfo = CurrentWeapon->WeaponInfo;

			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}

			if (InventoryComponent)
			{
				InventoryComponent->SwitchWeaponToIndex(OldIndex, OldInfo);
			}
		}
	}
}

void ASnowBall_TPSCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	PlayAnimMontage(Anim, 1.0f, "Mesh");

	WeaponReloadStart_BP(Anim);
}

void ASnowBall_TPSCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoSpent)
{
	if (!bIsSuccess)
		StopAnimMontage();

	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoSpent);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
	}

	WeaponReloadEnd_BP(bIsSuccess, AmmoSpent);
}

void ASnowBall_TPSCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if (InventoryComponent)
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);

	PlayAnimMontage(Anim, 1.0f, "Mesh");

	WeaponFireStart_BP(Anim);
}

void ASnowBall_TPSCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	//in BP
}

void ASnowBall_TPSCharacter::TryAbilityEnabled()
{
	if (AbilityEffect)
	{
		USnowBallTPS_StateEffect* NewEffect = NewObject<USnowBallTPS_StateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this); 
		}
	}
}

void ASnowBall_TPSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	//in BP
}

void ASnowBall_TPSCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess, int32 AmmoSpent)
{
	//in BP
}

void ASnowBall_TPSCharacter::MovementTick(float DeltaTime)
{
	if (bIsAllive)
	{
		APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

		if (myController)
		{
			MouseLocation, LineEnd;

			myController->DeprojectMousePositionToWorld(MouseLocation, LineEnd);
			LineEnd = (LineEnd * 5000) + MouseLocation;
			const FVector PointUnderCursor = FMath::LinePlaneIntersection(MouseLocation, LineEnd, GetActorLocation(), FVector(0, 0, 1));

			const float RotatingToCursor = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), PointUnderCursor).Yaw;

			if (!RollEnable)
			{
				AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
				AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

				SetActorRotation(FQuat(FRotator(0.0f, RotatingToCursor, 0.0f)));

				if (RunEnable)
				{
					const float ComparisonX = GetActorForwardVector().X;
					const float ComparisonY = GetActorForwardVector().Y;


					if (AxisX - 0.3f < ComparisonX && ComparisonX < AxisX + 0.3f
						&& AxisY - 0.3f < ComparisonY && ComparisonY < AxisY + 0.3f)
					{
						MoveSprint = true;
						ChangeMovementState();
					}
					else
					{
						MoveSprint = false;
						ChangeMovementState();
					}
				}

				if (AimEnable)
				{
					const FVector NewTransformLocation(GetActorLocation() + GetActorForwardVector() * CoefCameraOffset);
					NewRalativeLocation = FVector(NewTransformLocation.X, NewTransformLocation.Y, 198.150f);
					CameraBoom->SetWorldLocation(NewRalativeLocation);
				}

			}
			else
			{
				if (RollTime < MaxRollTime)
				{
					AddMovementInput(GetActorForwardVector());
					RollTime = RollTime + DeltaTime;
				}
				else
				{
					CurrentWeapon->bIsRoll = false;
					RollEnable = false;
					RollTime = 0.0f;
					ChangeMovementState();
				}
			}
		}
	}
}

void ASnowBall_TPSCharacter::CharacterUpdate()
{
	float ResSpeed = MovementInfo.BaseSpeed;

	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Base_State:
		ResSpeed = MovementInfo.BaseSpeed;
		break;
	case EMovementState::Roll_State:
		ResSpeed = MovementInfo.RollSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ASnowBall_TPSCharacter::ChangeMovementState()
{
	if (RollEnable)
	{
		MovementState = EMovementState::Roll_State;
	}
	else
	{
		if (AimEnable && !RollEnable)
		{
			MovementState = EMovementState::Aim_State;
		}
		else
		{
			if (WalkEnable && !AimEnable && !RollEnable)
			{
				MovementState = EMovementState::Walk_State;
			}
			else
			{
				if (RunEnable && MoveSprint && !WalkEnable && !AimEnable && !RollEnable)
				{
					MovementState = EMovementState::Run_State;
				}
				else
				{
					MovementState = EMovementState::Base_State;
				}
			}
		}
	}

	CharacterUpdate();

	//Weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

void ASnowBall_TPSCharacter::AttackCharEvent(bool bIsFire)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo check mele or range
		myWeapon->SetWeaponStateFire(bIsFire);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ASnowBall_TPSCharacter::AttackCharEvent - CurrentWeapon - NULL"));
}

AWeaponDefault* ASnowBall_TPSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ASnowBall_TPSCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	USnowBall_TPSGameInstance* myGI = Cast<USnowBall_TPSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;

	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawmParams;
				SpawmParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawmParams.Owner = this;
				SpawmParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation,
					&SpawnRotation, SpawmParams));

				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;

					myWeapon->myPawn = this;
					myWeapon->bIsPalyer = true;

					myWeapon->WeaponSetting = myWeaponInfo;
					//myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;

					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon(MovementState);

					myWeapon->WeaponInfo = WeaponAdditionalInfo;

					//if (InventoryComponent)
						CurrentIndexWeapon = InventoryComponent->GetWeaponIndexSlotByName(IdWeaponName);

					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ASnowBall_TPSCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ASnowBall_TPSCharacter::WeaponReloadEnd);
					myWeapon->OnWeaponFireStart.AddDynamic(this, &ASnowBall_TPSCharacter::WeaponFireStart);

					if (myWeapon->WeaponInfo.Round <=0)
					{
						TryReloadWeapon();
					}
				}
			}
		}
	}
}

void ASnowBall_TPSCharacter::AnimationDropMagazine()
{
	FTransform Transform;
	FActorSpawnParameters Param;

	Transform.SetRotation(CurrentWeapon->WeaponSetting.MagazineAnimRotator.Quaternion());

	Param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	Param.Owner = this;

	MagazineMeshAnimation = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Transform, Param);
	MagazineMeshAnimation->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
	MagazineMeshAnimation->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("NoCollision"));
	MagazineMeshAnimation->GetStaticMeshComponent()->SetStaticMesh(CurrentWeapon->WeaponSetting.MagazineDrop.DropMesh);

	MagazineMeshAnimation->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, "LeftHand");
}

void ASnowBall_TPSCharacter::AnimationDestroyMagazine()
{
	MagazineMeshAnimation->Destroy();
}


EPhysicalSurface ASnowBall_TPSCharacter::GetSurfaceType()
{
	EPhysicalSurface result = EPhysicalSurface::SurfaceType_Default;

	if (CharHealthComponent)
	{
		if (CharHealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
				if (myMaterial)
				{
					result = myMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}
	return result;
}

TArray<USnowBallTPS_StateEffect*> ASnowBall_TPSCharacter::GetCurrentEffects()
{
	return Effects;
}

void ASnowBall_TPSCharacter::RemoveEffect(USnowBallTPS_StateEffect* removeEffect)
{
	Effects.Remove(removeEffect);
}

void ASnowBall_TPSCharacter::AddEffect(USnowBallTPS_StateEffect* newEffect)
{
	Effects.Add(newEffect);
}

bool ASnowBall_TPSCharacter::CanAddStateEffect(TSubclassOf<USnowBallTPS_StateEffect> AddEffectClass)
{
	bool result = true;
	int i = 0;
	int32 MaxCoutEffect = 0;
	while (i < MaxCoutStateEffect.Num())
	{
		if (AddEffectClass == MaxCoutStateEffect[i].StateEffect)
		{
			MaxCoutEffect = MaxCoutStateEffect[i].Cout;
		}

		i++;
	}

	i = 0;
	int32 CoutEffect = 0;
	while (i < Effects.Num() && result == true)
	{
		if (Effects[i]->GetClass() == AddEffectClass)
		{
			CoutEffect++;
			if (CoutEffect > MaxCoutEffect)
			{
				result = false;
			}
		}
		i++;
	}

	return result;
}

FName ASnowBall_TPSCharacter::GetNameBoneForStateEffect(FTransform& EmitterTransform)
{
	FName BoneName = "none";

	if (GetMesh())
	{
		BoneName = GetMesh()->GetBoneName(0);
		EmitterTransform = GetMesh()->GetBoneTransform(0);
	}
	return BoneName;
}

void ASnowBall_TPSCharacter::CharDead_BP_Implementation()
{
	//BP
}


void ASnowBall_TPSCharacter::CharDead()
{
	CharHealthComponent->Live = false;
	float TimeAnim = 0.0f;
	bIsAllive = false;

	const int32 i = FMath::RandRange(0, DeathMontage.Num() - 1);

	if (DeathMontage.IsValidIndex(i) && DeathMontage[i] && GetMesh()->GetAnimInstance() && GetMesh())
	{
		TimeAnim = DeathMontage[i]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeathMontage[i]);
	}

	if(GetController())
	{
		GetController()->UnPossess();
	}


	AttackCharEvent(false);
	
	//Timer racdoll
	GetWorldTimerManager().SetTimer(TimerHandle_RacollTimer, this, &ASnowBall_TPSCharacter::EnableRacDoll, TimeAnim, false);

	GetCursorToWorld()->SetVisibility(false);

	CharDead_BP();
}

void ASnowBall_TPSCharacter::EnableRacDoll()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

float ASnowBall_TPSCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if (bIsAllive)
	{
		CharHealthComponent->ChangeHealthValue(-DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, myProjectile->ProjectileSetting.Effect, GetSurfaceType());
		}
	}


	//ISnowBallTPS_IGameActor* myInterface = Cast<ISnowBallTPS_IGameActor>(Hit.GetActor());
	//if (myInterface)
	//{
	//	//EPhysicalSurface mySurface;
	//	//mySurface = myInterface->GetSurfaceType();

	//	UTypes::AddEffectBySurfaceType(Hit.GetActor(), ProjectileInfo.Effect, mySurfacetype);

	//}

	return 	ActualDamage;
}

void ASnowBall_TPSCharacter::AimCameraReset()
{
	CameraBoom->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
}


