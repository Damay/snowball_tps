// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/SnowBall_TPSInventoryComponent.h"
#include "Game/SnowBall_TPSGameInstance.h"

// Sets default values for this component's properties
USnowBall_TPSInventoryComponent::USnowBall_TPSInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void USnowBall_TPSInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
	for (int8 i = 0; i < WeaponSlot.Num(); i++)
	{
		USnowBall_TPSGameInstance* myGI = Cast<USnowBall_TPSGameInstance>(GetWorld()->GetGameInstance());

		if (myGI)
		{
			if (!WeaponSlot[i].NameItem.IsNone())
			{
				FWeaponInfo Info;

				if (myGI->GetWeaponInfoByName(WeaponSlot[i].NameItem, Info))
				{
					WeaponSlot[i].AdditionalInfo.Round = Info.MaxRound;
				}
				else
				{
					//WeaponSlot.RemoveAt(i);
					//i--;
				}
			}
		}
	}

	MaxSlotWeapon = WeaponSlot.Num();

	if (WeaponSlot.IsValidIndex(0))
	{
		if (!WeaponSlot[0].NameItem.IsNone())
			OnSwichWeapon.Broadcast(WeaponSlot[0].NameItem, WeaponSlot[0].AdditionalInfo, 0);
	}
}


// Called every frame
void USnowBall_TPSInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool USnowBall_TPSInventoryComponent::SwitchWeaponToIndex(int32 OldIndex, FAdditionalWeaponInfo OldInfo)
{
	bool bIsSuccess = false;
	int8 ChangedWeapon = 0;
	
	FName NewNameWeapon;
	FAdditionalWeaponInfo NewAdditionalInfo;

	int32 ChangeToIndex = OldIndex + 1;

	WeaponSlot[OldIndex].AdditionalInfo = OldInfo;

	/*if (IndexWeaponToSwitch > MaxSlotWeapon)
	{*/
		while (!bIsSuccess && ChangedWeapon < WeaponSlot.Num())
		{
			if (ChangeToIndex == WeaponSlot.Num())
			{
				ChangeToIndex = 0;
			}

			if (!WeaponSlot[ChangeToIndex].NameItem.IsNone())
			{
				if (AmmoSlot[ChangeToIndex].Cout != 0 || WeaponSlot[ChangeToIndex].AdditionalInfo.Round != 0)
				{
					NewNameWeapon = WeaponSlot[ChangeToIndex].NameItem;
					NewAdditionalInfo = WeaponSlot[ChangeToIndex].AdditionalInfo;
					bIsSuccess = true;
					ChangedWeapon = 0;
				}
			}

			ChangedWeapon++;
			ChangeToIndex++;
		}
	/* }
	else
	{
		ChangeToIndex = IndexWeaponToSwitch;

		if (!WeaponSlot[ChangeToIndex].NameItem.IsNone())
		{
			if (AmmoSlot[ChangeToIndex].Cout != 0 || WeaponSlot[ChangeToIndex].AdditionalInfo.Round != 0)
			{
				NewNameWeapon = WeaponSlot[ChangeToIndex].NameItem;
				NewAdditionalInfo = WeaponSlot[ChangeToIndex].AdditionalInfo;
				bIsSuccess = true;
			}
		}
	}*/

	if (!bIsSuccess)
	{
		//What will do, if switch not success
	}

	if (bIsSuccess)
	{
		SetAdditionalInfoWeapon(OldIndex, OldInfo);
		OnSwichWeapon.Broadcast(NewNameWeapon, NewAdditionalInfo, ChangeToIndex - 1);
	}

	return bIsSuccess;
}

FAdditionalWeaponInfo USnowBall_TPSInventoryComponent::GetAdditionalinfoWeapon(int32 IndexWeapon)
{
	FAdditionalWeaponInfo result;

	if (WeaponSlot.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlot.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				result = WeaponSlot[i].AdditionalInfo;
				bIsFind = true;
			}
			i++;
		}
		if (!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT(" FAdditionalWeaponInfo USnowBall_TPSInventoryComponent::GetAdditionalinfoWeapon - No found weapon with inex - %d"), IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT(" FAdditionalWeaponInfo USnowBall_TPSInventoryComponent::GetAdditionalinfoWeapon - Not correct index weapon - %d"), IndexWeapon);

	return result;
}

int32 USnowBall_TPSInventoryComponent::GetWeaponIndexSlotByName(FName IdWeapon)
{
	int32 result = -1;
	int8 i = 0;
	bool bIsFind = false;

	while (i < WeaponSlot.Num() && !bIsFind)
	{
		if (WeaponSlot[i].NameItem == IdWeapon)
		{
			result = i;
			bIsFind = true;
		}
		i++;
	}

	return result;
}

FName USnowBall_TPSInventoryComponent::GetWeaponNameSlotByIndex(int32 IndexWeapon)
{
	if (WeaponSlot.IsValidIndex(IndexWeapon))
	{
		return WeaponSlot[IndexWeapon].NameItem;
	}
	return "";
}

void USnowBall_TPSInventoryComponent::SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	if (WeaponSlot.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;

		while (i < WeaponSlot.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				WeaponSlot[i].AdditionalInfo = NewInfo;
				bIsFind = true;

				OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo);
			}
			i++;
		}
		if (!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT(" USnowBall_TPSInventoryComponent::SetAdditionalInfoWeapon - No found weapon with inex - %d"), IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT(" USnowBall_TPSInventoryComponent::SetAdditionalInfoWeapon - Not correct index weapon - %d"), IndexWeapon);
}

void USnowBall_TPSInventoryComponent::AmmoSlotChangeValue(EWeaponType WeaponType, int32 CoutChangeAmmo)
{
	bool bIsFind = false;
	int i = 0;
	
	while (i < AmmoSlot.Num() && !bIsFind)
	{
		if (AmmoSlot[i].WeaponType == WeaponType)
		{
			AmmoSlot[i].Cout += CoutChangeAmmo;

			if (AmmoSlot[i].Cout > AmmoSlot[i].MaxCout)
				AmmoSlot[i].Cout = AmmoSlot[i].MaxCout;

			if (AmmoSlot[i].Cout < 0)
			{
				AmmoSlot[i].Cout = 0;
			}

			OnAmmoChange.Broadcast(WeaponType, AmmoSlot[i].Cout);

			if (CoutChangeAmmo > 0)
				OnAddedAmmo.Broadcast(WeaponType);

			bIsFind = true;
		}
		i++;
	}
}

bool USnowBall_TPSInventoryComponent::CheckAmmoForWeapon(EWeaponType TypeWeapon, int8 & AvailableAmmoForWeapon)
{
	AvailableAmmoForWeapon = 0;

	bool bIsFind = false;
	int32 i = 0;

	while (i < AmmoSlot.Num() && !bIsFind)
	{
		if (AmmoSlot[i].WeaponType == TypeWeapon)
		{
			bIsFind = true;

			AvailableAmmoForWeapon = AmmoSlot[i].Cout;

			if (AmmoSlot[i].Cout > 0)
				return true;
		}

		i++;
	}

	return false;
}

bool USnowBall_TPSInventoryComponent::CheckCanTakeAmmo(EWeaponType AmmoType)
{
	bool result = false;
	int8 i = 0;
	while (i < AmmoSlot.Num() && !result)
	{
		if (AmmoSlot[i].WeaponType == AmmoType && AmmoSlot[i].Cout < AmmoSlot[i].MaxCout)
		{
			result = true;
		}

		i++;
	}
	return result;
}

bool USnowBall_TPSInventoryComponent::CheckCanTakeWeapon(int32 &FreeSlot)
{
	bool bIsFreeSlot = false;
	int8 i = 0;

	while (i < WeaponSlot.Num() && !bIsFreeSlot)
	{
		if (WeaponSlot[i].NameItem.IsNone())
		{
			FreeSlot = i;
			bIsFreeSlot = true;
		}
		i++;
	}
	return bIsFreeSlot;
}

bool USnowBall_TPSInventoryComponent::SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, FDropItem& DropItemInfo)
{
	if (WeaponSlot.IsValidIndex(IndexSlot) && GetDropitemInfoFromInventory(IndexSlot, DropItemInfo))
	{
		int8 i = 0;

		while (i < WeaponSlot.Num())
		{
			if (WeaponSlot[i].NameItem == NewWeapon.NameItem)
			{
				return false;
			}

			i++;
		}
		WeaponSlot[IndexSlot] = NewWeapon;

		SwitchWeaponToIndex(IndexSlot, NewWeapon.AdditionalInfo);

		OnUpdateWeaponSlots.Broadcast(IndexSlot, NewWeapon);
		return true;
	}
	return false;
}

bool USnowBall_TPSInventoryComponent::GetDropitemInfoFromInventory(int32 IndexDropSlot, FDropItem& DropItemInfo)
{
	bool result = false;
	FName DropItemName = GetWeaponNameSlotByIndex(IndexDropSlot);
	
	USnowBall_TPSGameInstance* myGi = Cast<USnowBall_TPSGameInstance>(GetWorld()->GetGameInstance());
	if (myGi)
	{
		result = myGi->GetDropItemInfoByName(DropItemName, DropItemInfo);
		if (WeaponSlot.IsValidIndex(IndexDropSlot))
		{
			DropItemInfo.WeaponInfo.AdditionalInfo = WeaponSlot[IndexDropSlot].AdditionalInfo;
		}

	}

	return result;
}

bool USnowBall_TPSInventoryComponent::TryGetWeaponToInventory(FWeaponSlot NewWeapon)
{
	int32 IndexSlot = -1;

	int8 i = 0;

	while (i < WeaponSlot.Num())
	{
		if (WeaponSlot[i].NameItem == NewWeapon.NameItem)
		{
			return false;
		}

		i++;
	}

	if (CheckCanTakeWeapon(IndexSlot))
	{
		if (WeaponSlot.IsValidIndex(IndexSlot))
		{
			WeaponSlot[IndexSlot] = NewWeapon;
			OnUpdateWeaponSlots.Broadcast(IndexSlot, NewWeapon);
			return true;
		}
	}
	return false;
}

