// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Weapon/Weapon/WeaponDefault.h"
#include "FuncLibrary/Types.h"
#include "Engine/StaticMeshActor.h"
#include "SnowBall_TPSInventoryComponent.h"
#include "SnowBall_TPSCharHealthComponent.h"
#include "Interface/SnowBallTPS_IGameActor.h"
#include "StateEffect/SnowBallTPS_StateEffect.h"
#include "SnowBall_TPSCharacter.generated.h"


UCLASS(Blueprintable)
class ASnowBall_TPSCharacter : public ACharacter, public ISnowBallTPS_IGameActor
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:
	ASnowBall_TPSCharacter();

	FTimerHandle TimerHandle_RacollTimer;
	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	//FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }
	FORCEINLINE class USnowBall_TPSInventoryComponent* GetInventoryComponent() const { return InventoryComponent; }
	FORCEINLINE class USnowBall_TPSCharHealthComponent* GetCharHealthComponent() const { return CharHealthComponent; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USnowBall_TPSInventoryComponent* InventoryComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USnowBall_TPSCharHealthComponent* CharHealthComponent;
	/** A decal that projects to the cursor location. */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	//class UDecalComponent* CursorToWorld;

public:
	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

	UDecalComponent* CurrentCursor = nullptr;

	UFUNCTION(BlueprintCallable)
	UDecalComponent* GetCursorToWorld();

	//Movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Start state")
	EMovementState MovementState = EMovementState::Base_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementInfo;

	UPROPERTY(BlueprintReadWrite)
		bool RunEnable = false;
	UPROPERTY(BlueprintReadWrite)
		bool WalkEnable = false;
	UPROPERTY(BlueprintReadWrite)
		bool AimEnable = false;
	UPROPERTY(BlueprintReadWrite)
		bool RollEnable = false;
	UPROPERTY(BlueprintReadWrite)
		float RollTime = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float MaxRollTime = 0.9f;
	UPROPERTY(BlueprintReadWrite)
		bool MoveSprint = false;
	UPROPERTY(BlueprintReadWrite)
		bool bIsAllive = true;

	//Effect
	TArray<USnowBallTPS_StateEffect*> Effects;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StateEffect")
	TArray<FMaxCoutStateEffects> MaxCoutStateEffect;

	//Input
	UFUNCTION()
	void InputAxisX(float Value);
	UFUNCTION()
	void InputAxisY(float Value);
	UFUNCTION()
	void InputAttackPressed();
	UFUNCTION()
	void InputAttackReleased();

	//Weapon
	UFUNCTION()
	void TryReloadWeapon();
	UFUNCTION(BlueprintCallable)
	void TrySwitchWeapon();
	UFUNCTION()
	void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
	void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSpent);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP(bool bIsSuccess, int32 AmmoSpent);
	UFUNCTION()
	void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponFireStart_BP(UAnimMontage* Anim);

	//Ability
	UFUNCTION()
	void TryAbilityEnabled();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityEffect")
	TSubclassOf<USnowBallTPS_StateEffect> AbilityEffect;


	float AxisX = 0.0f;
	float AxisY = 0.0f;

	//Tick function
	UFUNCTION()
	void MovementTick(float DeltaTime);
	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
	void AimCameraReset();
	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();
	UFUNCTION(BlueprintCallable)
	void AttackCharEvent(bool bIsFire);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
	float CoefCameraOffset = 100.0f;

	UPROPERTY(BlueprintReadWrite)
	FVector NewRalativeLocation;

	//Weapon
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Current weapon")
	AWeaponDefault* CurrentWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimMontage")
	TArray<UAnimMontage*> DeathMontage;

	UFUNCTION(BlueprintCallable)
	AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	int32 CurrentIndexWeapon = 0;

	//Shoot
	FVector MouseLocation;
	FVector LineEnd;

	//Magazine animation
	UFUNCTION(BlueprintCallable)
	void AnimationDropMagazine();
	UFUNCTION(BlueprintCallable)
	void AnimationDestroyMagazine();
	AStaticMeshActor* MagazineMeshAnimation = nullptr;

	//Interface
	EPhysicalSurface GetSurfaceType() override;
	TArray<USnowBallTPS_StateEffect*> GetCurrentEffects() override;
	void RemoveEffect(USnowBallTPS_StateEffect* removeEffect) override;
	void AddEffect(USnowBallTPS_StateEffect* newEffect) override;
	bool CanAddStateEffect(TSubclassOf<USnowBallTPS_StateEffect> AddEffectClass) override;
	FName GetNameBoneForStateEffect(FTransform& EmitterTransform);
	//EndInterface

	UFUNCTION(BlueprintNativeEvent)
	void CharDead_BP();

	UFUNCTION()
	void CharDead();
	void EnableRacDoll();
	float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
};