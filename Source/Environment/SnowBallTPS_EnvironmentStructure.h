// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interface/SnowBallTPS_IGameActor.h"
#include "StateEffect/SnowBallTPS_StateEffect.h"
#include "FuncLibrary/Types.h"
#include "Character/SnowBall_TPSHealthComponent.h"
#include "SnowBallTPS_EnvironmentStructure.generated.h"

UCLASS()
class SNOWBALL_TPS_API ASnowBallTPS_EnvironmentStructure : public AActor, public ISnowBallTPS_IGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnowBallTPS_EnvironmentStructure();

	FORCEINLINE class USnowBall_TPSHealthComponent* GetHealthComponent() const { return HealthComponent; }


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USnowBall_TPSHealthComponent* HealthComponent;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	EPhysicalSurface GetSurfaceType() override;

	TArray<USnowBallTPS_StateEffect*> GetCurrentEffects() override;
	void RemoveEffect(USnowBallTPS_StateEffect* removeEffect) override;
	void AddEffect(USnowBallTPS_StateEffect* newEffect) override;
	bool CanAddStateEffect(TSubclassOf<USnowBallTPS_StateEffect> AddEffectClass) override;
	FName GetNameBoneForStateEffect(FTransform& EmitterTransform) override;

	//Effect
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TArray<USnowBallTPS_StateEffect*> Effects;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StateEffect")
	TArray<FMaxCoutStateEffects> MaxCoutStateEffect;

	UFUNCTION()
	void Dead();
	UFUNCTION(BlueprintNativeEvent)
	void Dead_BP();

};
