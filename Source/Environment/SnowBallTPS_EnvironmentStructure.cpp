// Fill out your copyright notice in the Description page of Project Settings.


#include "SnowBallTPS_EnvironmentStructure.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets default values
ASnowBallTPS_EnvironmentStructure::ASnowBallTPS_EnvironmentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	HealthComponent = CreateDefaultSubobject<USnowBall_TPSHealthComponent>(TEXT("HealthComponent"));
}

// Called when the game starts or when spawned
void ASnowBallTPS_EnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
	
	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &ASnowBallTPS_EnvironmentStructure::Dead);
	}
}

// Called every frame
void ASnowBallTPS_EnvironmentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ASnowBallTPS_EnvironmentStructure::GetSurfaceType()
{
	EPhysicalSurface result = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));

	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if (myMaterial)
		{
			result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}

	return result;
}

TArray<USnowBallTPS_StateEffect*> ASnowBallTPS_EnvironmentStructure::GetCurrentEffects()
{
	return Effects;
}

void ASnowBallTPS_EnvironmentStructure::RemoveEffect(USnowBallTPS_StateEffect* removeEffect)
{
	Effects.Remove(removeEffect);
}

void ASnowBallTPS_EnvironmentStructure::AddEffect(USnowBallTPS_StateEffect* newEffect)
{
	Effects.Add(newEffect);
}

bool ASnowBallTPS_EnvironmentStructure::CanAddStateEffect(TSubclassOf<USnowBallTPS_StateEffect> AddEffectClass)
{
	bool result = true;
	int i = 0;
	int32 MaxCoutEffect = 0;
	while (i < MaxCoutStateEffect.Num())
	{
		if (AddEffectClass == MaxCoutStateEffect[i].StateEffect)
		{
			MaxCoutEffect = MaxCoutStateEffect[i].Cout;
		}

		i++;
	}
	
	i = 0;
	int32 CoutEffect = 0;
	while (i < Effects.Num() && result == true)
	{
		if (Effects[i]->GetClass() == AddEffectClass)
		{
			CoutEffect++;
			if (CoutEffect >= MaxCoutEffect)
			{
				result = false;
			}
		}
		i++;
	}

	return result;
}

FName ASnowBallTPS_EnvironmentStructure::GetNameBoneForStateEffect(FTransform& EmitterTransform)
{
	EmitterTransform = this->GetActorTransform();

	return "none";
}

void ASnowBallTPS_EnvironmentStructure::Dead()
{
	int i = 0;
	while (i < Effects.Num())
	{
		if (Effects[i])
		{
			Effects[i]->DestroyObject();
		}
	}
	
	Dead_BP();
}

void ASnowBallTPS_EnvironmentStructure::Dead_BP_Implementation()
{

}
