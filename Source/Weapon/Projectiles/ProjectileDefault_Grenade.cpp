// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/Projectiles/ProjectileDefault_Grenade.h"
#include "Math/Vector.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

int32 DebugExplodeShow = 0;
FAutoConsoleVariableRef CVARExplodeShow(
	TEXT("ASnowBall_TPS.DebugExplode"),
	DebugExplodeShow,
	TEXT("Draw Debug for Explode"),
	ECVF_Cheat);

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);
}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > ProjectileSetting.TimeToExplose)
		{
			Explose();
		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);

	//ImpactProjectile();
}

void AProjectileDefault_Grenade::ImpactProjectile()
{	
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::InitProjectile(FProjectileInfo InitParam)
{
	Super::InitProjectile(InitParam);
}

void AProjectileDefault_Grenade::Explose()
{
	TimerEnabled = false;
	if (ProjectileSetting.ExploseFX)
	{
		FVector VectorToExplose = FVector(0.0f, 0.0f, 0.0f);
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExploseFX, GetActorLocation(), 
			VectorToExplose.Rotation());
	}
	if (ProjectileSetting.ExploseSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExploseSound, GetActorLocation(), 0.3f);
	}

	TArray<AActor*> IgnoredActor;

	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExploseMaxDamage, ProjectileSetting.ExploseMaxDamage * 0.2f,
		GetActorLocation(), ProjectileSetting.ProjectileInnerRadiusDamage, ProjectileSetting.ProjectileMaxRadiusDamage, 5, NULL, IgnoredActor, this, nullptr);

	if (DebugExplodeShow)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileInnerRadiusDamage, 14, FColor(181, 0, 0), true, -1, 0, 2);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, 28, FColor(0, 0, 181), true, -1, 0, 2);
	}

	this->Destroy();
}
