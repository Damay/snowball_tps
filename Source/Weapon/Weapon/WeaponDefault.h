// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "FuncLibrary/Types.h"
#include "Weapon/Projectiles/ProjectileDefault.h"
#include "WeaponDefault.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, Anim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoSpent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, AnimMontage);

UCLASS()
class SNOWBALL_TPS_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;
	FOnWeaponFireStart OnWeaponFireStart;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShootLocation = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShellDropLocation = nullptr;

	UPROPERTY()
	FWeaponInfo WeaponSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	FAdditionalWeaponInfo WeaponInfo;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	
	void WeaponInit();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool WeaponFiring = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Reloading")
	bool WeaponReloading = false;

	void SetWeaponStateFire(bool bIsFire);

	UFUNCTION(BlueprintCallable)
	bool CheckWeaponCanFire();

	void Fire();

	//ShotLocation
	void FindEndLocation();
	FVector ShotEndLocation = FVector(0.0f);
	float SizeChanelToChangeShootDirectionLogic = 120.0f;

	void UpdateStateWeapon(EMovementState NewMovementState);

	float CoefDispersion = 0.0f;

	// Timers flag
	float FireTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Reloading")
	float ReloadTimer = 0.0f;
	//Remove
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Reloading")
	float ReloadTime = 0.0f;

	//Shoot direction
	class ASnowBall_TPSCharacter* myPawn = nullptr;
	bool bIsPalyer = false;

	//Block fire when the character rolls
	bool bIsRoll = false;

	bool WeaponAiming;

	UFUNCTION()
	void InitDropMesh(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh,
			float ImpulseRandomDispersion, float PowerImpulse, float CustomMass);

	//DropShellBullet
	bool DropShellFlag = false;
	float DropShellTimer = -1.0f;
	void MagazineDropTick(float DelataTime);

	//DropMagazine
	bool DropMagazineFlag = false;
	float DropMagazineTimer = -1.0f;
	void ShellBulletDropTick(float DelataTime);

	//Confusion
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	int Confusion = 2;

	UFUNCTION(BlueprintCallable)
	int32 GetWeaponRound();

	FProjectileInfo GetProjectile();

	void StartReload();
	void EndReload();

	uint8 GetNumberProjectileByShot() const;

	//CancelReload
	UFUNCTION(BlueprintCallable)
	void CancelReload();
	bool MagazineIsDroped = false;

	bool CheckWeaponReload();
	int8 GetAviableAmmoForReload();
};
