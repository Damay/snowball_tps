// Fill out your copyright notice in the Description page of Project Settings.

#include "WeaponDefault.h"
#include "Character/SnowBall_TPSCharacter.h"
#include "Kismet/KismetMathLibrary.h"
#include "Animation/AnimInstance.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Math/Vector.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"
#include "StateEffect/SnowBallTPS_StateEffect.h"
#include "Character/SnowBall_TPSInventoryComponent.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Create RootComponent
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	//Create Skeletal mesh
	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	//Create Static mesh
	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	//Create Shoot location
	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);

	//Create Shell drop location
	ShellDropLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShellDropLocation"));
	ShellDropLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit();
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	MagazineDropTick(DeltaTime);
	ShellBulletDropTick(DeltaTime);
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if (FireTimer > 0.0f)
		FireTimer -= DeltaTime;
	else
	{
		if (WeaponFiring && !WeaponReloading && !bIsRoll && WeaponInfo.Round != 0)
			Fire();
	}
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading)
	{
		if (ReloadTimer <= 0.0f)
			EndReload();
		else
			ReloadTimer -= DeltaTime;
	}
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}
	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent(true);
	}
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire())
	{
		WeaponFiring = bIsFire;
	}
	else
		WeaponFiring = false;
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	if (bIsRoll)
	{
		return false;
	}
	return true;
}

void AWeaponDefault::Fire()
{
	UAnimMontage* AnimMontage = nullptr;

	if (WeaponAiming)
		AnimMontage = WeaponSetting.AnimWeaponInfo.AimAnimCharFire;
	else
		AnimMontage = WeaponSetting.AnimWeaponInfo.AnimCharFire;

	if (WeaponSetting.ShellBulletDrop.DropMesh)
	{
		if (WeaponSetting.ShellBulletDrop.DropMeshTime < 0.0f)
		{
			InitDropMesh(WeaponSetting.ShellBulletDrop.DropMesh, WeaponSetting.ShellBulletDrop.DropMeshOffset, WeaponSetting.ShellBulletDrop.DropMeshImpulseDir,
				WeaponSetting.ShellBulletDrop.DropMeshLifeTime, WeaponSetting.ShellBulletDrop.ImpulseRanomDirection, WeaponSetting.ShellBulletDrop.PowerImpulse, WeaponSetting.ShellBulletDrop.CustomMass);
		}
		else
		{
			DropShellFlag = true;
			DropShellTimer = WeaponSetting.ShellBulletDrop.DropMeshTime;
		}
	}

	uint8 NumberProjectile = GetNumberProjectileByShot();
	FireTimer = WeaponSetting.RateOfFire;

	WeaponInfo.Round = WeaponInfo.Round - 1 * NumberProjectile;
	
	OnWeaponFireStart.Broadcast(AnimMontage);

	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, ShootLocation->GetComponentLocation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, ShootLocation->GetComponentTransform());

	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();

		FindEndLocation();
		
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();

		if (ProjectileInfo.Projectile)
		{

			float RotationToTarget = UKismetMathLibrary::FindLookAtRotation(SpawnLocation, ShotEndLocation).Yaw;
			float RotationToTargetWithDispersion = RotationToTarget + UKismetMathLibrary::RandomFloatInRange(-CoefDispersion, CoefDispersion);

			if (WeaponSetting.AnimWeaponInfo.AnimWeaponFire
				&& SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
			{
				SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(WeaponSetting.AnimWeaponInfo.AnimWeaponFire);
			}

			int8 CoefConfusionShot = (NumberProjectile / 2 )* - Confusion;
			FActorSpawnParameters SpawmParams;
			SpawmParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawmParams.Owner = GetOwner();
			SpawmParams.Instigator = GetInstigator();

			for (int8 i = 0; i < NumberProjectile; i++)
			{
				SpawnRotation = FRotator(0.0f, RotationToTargetWithDispersion + CoefDispersion*CoefConfusionShot, 0.0f);

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawmParams));
				if (myProjectile)
				{
					myProjectile->InitProjectile(ProjectileInfo);
				}

				CoefConfusionShot = CoefConfusionShot + Confusion;
			}
		}
		else
		{
			FHitResult Hit;
			TArray<AActor*> Actors;

			UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, SpawnLocation + ShootLocation->GetForwardVector()*WeaponSetting.DistanceTrace,
				ETraceTypeQuery::TraceTypeQuery4, false, Actors, EDrawDebugTrace::ForDuration, Hit, true, FLinearColor::Green, FLinearColor::Red, 5.0f);

			if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
			{
				EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);

				if (WeaponSetting.ProjectileSetting.HitDecal.Contains(mySurfacetype))
				{
					UMaterialInterface* myMaterial = WeaponSetting.ProjectileSetting.HitDecal[mySurfacetype];

					if (myMaterial && Hit.GetComponent())
					{
						UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.0f), Hit.GetComponent(), NAME_None, Hit.ImpactPoint,
							Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
					}
				}
				if (WeaponSetting.ProjectileSetting.HitFXs.Contains(mySurfacetype))
				{
					UParticleSystem* myParticle = WeaponSetting.ProjectileSetting.HitFXs[mySurfacetype];

					if (myParticle)
					{
						UGameplayStatics::SpawnEmitterAttached(myParticle, Hit.GetComponent(), NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(),
							EAttachLocation::KeepWorldPosition);
					}
				}
				if (WeaponSetting.ProjectileSetting.HitSound)
				{
					UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSetting.ProjectileSetting.HitSound, Hit.ImpactPoint, 0.3f);
				}

				UTypes::AddEffectBySurfaceType(Hit.GetActor(), ProjectileInfo.Effect, mySurfacetype);

				UGameplayStatics::ApplyPointDamage(Hit.GetActor(), WeaponSetting.ProjectileSetting.ProjectileDamage, Hit.ImpactPoint, Hit, NULL, this, nullptr);
			}
		}
	}

	if (GetWeaponRound() <= 0 && !WeaponReloading)
	{
		if (CheckWeaponReload())
		{
			StartReload();
		}
		else
		{
			myPawn->TrySwitchWeapon();
			myPawn->GetInventoryComponent()->OnAmmoTypeEmpty.Broadcast(WeaponSetting.WeaponType);
		}
	}
}

void AWeaponDefault::FindEndLocation()
{
	//if (bIsPalyer) // ShootTarget
	//{
	if (myPawn)
	{
		FVector DecalLocation = FMath::LinePlaneIntersection(myPawn->MouseLocation, myPawn->LineEnd, ShootLocation->GetComponentLocation(), FVector(0, 0, 1));
		FVector tmpV = (myPawn->GetActorLocation() - DecalLocation);

		if (tmpV.Size() > SizeChanelToChangeShootDirectionLogic)
		{
			ShotEndLocation = DecalLocation;
		}
		else
		{
			ShotEndLocation = ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 2000.0f;
		}
		
	}
	//}
	//else
	//{
		//ToDo for enemy
	//}
}


void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{
	//ToDo Dispersion
	if (!bIsRoll)
	{
		switch (NewMovementState)
		{
		case EMovementState::Aim_State:
			CoefDispersion = WeaponSetting.DispersionWeapon.DispersionAim;
			WeaponAiming = true;
			break;
		case EMovementState::Base_State:
			CoefDispersion = WeaponSetting.DispersionWeapon.DispersionBase;
			WeaponAiming = false;
			break;
		case EMovementState::Walk_State:
			CoefDispersion = WeaponSetting.DispersionWeapon.DispersionWalk;
			WeaponAiming = false;
			break;
		case EMovementState::Run_State:
			CoefDispersion = WeaponSetting.DispersionWeapon.DispersionRun;
			WeaponAiming = false;
			break;
		case EMovementState::Roll_State:
			WeaponAiming = false;
			bIsRoll = true;
			break;
		default:
			break;
		}
	}
}

void AWeaponDefault::InitDropMesh(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass)
{
	if (DropMesh)
	{
		FTransform Transform;
		
		FVector LocalDir = this->GetActorForwardVector() * Offset.GetLocation().X + this->GetActorRightVector() * Offset.GetLocation().Y
			+ this->GetActorUpVector() * Offset.GetLocation().Z;

		Transform.SetLocation(GetActorLocation() + LocalDir);
		Transform.SetScale3D(Offset.GetScale3D());
		Transform.SetRotation((GetActorRotation() + Offset.Rotator()).Quaternion());

		AStaticMeshActor* NewActor = nullptr;

		FActorSpawnParameters Param;
		Param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		Param.Owner = this;
		NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Transform, Param);

		if (NewActor && NewActor->GetStaticMeshComponent())
		{
			NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
			NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
			NewActor->SetActorTickEnabled(false);
			NewActor->InitialLifeSpan = LifeTimeMesh;
			NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
			NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
			NewActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);
				
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel11, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel12, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);

			if (CustomMass > 0.0f)
			{
				NewActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, CustomMass, true);
			}

			if (!DropImpulseDirection.IsNearlyZero())
			{
				FVector FinalDir;

				if (DropMesh == WeaponSetting.ShellBulletDrop.DropMesh)
					LocalDir = LocalDir + (ShellDropLocation->GetForwardVector() * 1000);

				LocalDir = LocalDir + (DropImpulseDirection * 1000);

				//FinalDir = ShellDropLocation->GetComponentLocation() + (DropImpulseDirection * 1000.0f);

				if (!FMath::IsNearlyZero(ImpulseRandomDispersion))
					FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, ImpulseRandomDispersion);

				FinalDir.GetSafeNormal(0.0001f);

				NewActor->GetStaticMeshComponent()->AddImpulse(FinalDir * PowerImpulse);
			}
		}
	}
}

void AWeaponDefault::MagazineDropTick(float DelataTime)
{
	if (DropMagazineFlag)
	{
		if (DropMagazineTimer < 0.0f)
		{
			DropMagazineFlag = false;
			InitDropMesh(WeaponSetting.MagazineDrop.DropMesh, WeaponSetting.MagazineDrop.DropMeshOffset, WeaponSetting.MagazineDrop.DropMeshImpulseDir,
				WeaponSetting.MagazineDrop.DropMeshLifeTime, WeaponSetting.MagazineDrop.ImpulseRanomDirection, WeaponSetting.MagazineDrop.PowerImpulse, WeaponSetting.MagazineDrop.CustomMass);
		}
		else
			DropMagazineTimer -= DelataTime;
	}
}

void AWeaponDefault::ShellBulletDropTick(float DelataTime)
{
	if (DropShellFlag)
	{
		if (DropShellTimer < 0.0f)
		{
			DropShellFlag = false;
			InitDropMesh(WeaponSetting.ShellBulletDrop.DropMesh, WeaponSetting.ShellBulletDrop.DropMeshOffset, WeaponSetting.ShellBulletDrop.DropMeshImpulseDir,
				WeaponSetting.ShellBulletDrop.DropMeshLifeTime, WeaponSetting.ShellBulletDrop.ImpulseRanomDirection, WeaponSetting.ShellBulletDrop.PowerImpulse, WeaponSetting.ShellBulletDrop.CustomMass);
		}
		else
			DropShellTimer -= DelataTime;
	}
}

int32 AWeaponDefault::GetWeaponRound()
{
	return WeaponInfo.Round;
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSetting.ProjectileSetting;
}

void AWeaponDefault::StartReload()
{
	WeaponReloading = true;
	ReloadTimer = WeaponSetting.ReloadTime;

	UAnimMontage* AnimToPlay;
	if (WeaponAiming)
		AnimToPlay = WeaponSetting.AnimWeaponInfo.AimAnimCharReload;
	else
		AnimToPlay = WeaponSetting.AnimWeaponInfo.AnimCharReload;

	if (WeaponSetting.AnimWeaponInfo.AnimWeaponFire
		&& SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(WeaponSetting.AnimWeaponInfo.AnimWeaponReload);
	}

	if (MagazineIsDroped == false)
	{
		InitDropMesh(WeaponSetting.MagazineDrop.DropMesh, WeaponSetting.MagazineDrop.DropMeshOffset, WeaponSetting.MagazineDrop.DropMeshImpulseDir,
			WeaponSetting.MagazineDrop.DropMeshLifeTime, WeaponSetting.MagazineDrop.ImpulseRanomDirection, WeaponSetting.MagazineDrop.PowerImpulse, WeaponSetting.MagazineDrop.CustomMass);
		
		//ToDo logic reloading when magazine mesh droped, but magazine not erase

		MagazineIsDroped = true;
	}
	
	OnWeaponReloadStart.Broadcast(AnimToPlay);
}

void AWeaponDefault::EndReload()
{
	WeaponReloading = false;

	int8 AviableAmmoFromInventory = GetAviableAmmoForReload();

	if (AviableAmmoFromInventory > WeaponSetting.MaxRound)
		AviableAmmoFromInventory = WeaponSetting.MaxRound;

	const int32 AmmoTaken = WeaponSetting.MaxRound - WeaponInfo.Round;

	if(WeaponInfo.Round + AviableAmmoFromInventory > WeaponSetting.MaxRound)
	{
		WeaponInfo.Round = WeaponSetting.MaxRound;
	}
	else
	{
		WeaponInfo.Round = WeaponInfo.Round + AviableAmmoFromInventory;
	}

	MagazineIsDroped = false;

	OnWeaponReloadEnd.Broadcast(true, -AmmoTaken);
}

uint8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSetting.NumberProjectileByShot;
}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;

	if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);

	OnWeaponReloadEnd.Broadcast(false, 0);
	DropShellFlag = false;
}

bool AWeaponDefault::CheckWeaponReload()
{
	bool result = true;

	if (GetOwner())
	{
		USnowBall_TPSInventoryComponent* myInventory = Cast<USnowBall_TPSInventoryComponent>(GetOwner()->GetComponentByClass(USnowBall_TPSInventoryComponent::StaticClass()));
		if (myInventory)
		{
			int8 AviableAmmoForWeapon;

			if (!myInventory->CheckAmmoForWeapon(WeaponSetting.WeaponType, AviableAmmoForWeapon))
			{
				result = false;
			}
		}
	}
	return result;
}

int8 AWeaponDefault::GetAviableAmmoForReload()
{
	int8 AvailableAmmoForWeapon = 0;

	if (GetOwner())
	{
		USnowBall_TPSInventoryComponent* myInventory = Cast<USnowBall_TPSInventoryComponent>(GetOwner()->GetComponentByClass(USnowBall_TPSInventoryComponent::StaticClass()));
		if (myInventory)
		{
		//	int8 AvailableChackAmmoForWeapon = 0;

			if (myInventory->CheckAmmoForWeapon(WeaponSetting.WeaponType, /*AvailableChackAmmoForWeapon*/ AvailableAmmoForWeapon))
			{
				AvailableAmmoForWeapon = /*AvailableChackAmmoForWeapon*/ AvailableAmmoForWeapon;
			}
		}
	}
	
	return AvailableAmmoForWeapon;
}

