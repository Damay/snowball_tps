// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnowBall_TPSGameMode.h"
#include "SnowBall_TPSPlayerController.h"
#include "Character/SnowBall_TPSCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASnowBall_TPSGameMode::ASnowBall_TPSGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ASnowBall_TPSPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
