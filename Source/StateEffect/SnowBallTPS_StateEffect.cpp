// Fill out your copyright notice in the Description page of Project Settings.


#include "StateEffect/SnowBallTPS_StateEffect.h"
#include "Kismet/GameplayStatics.h"
#include "Interface/SnowBallTPS_IGameActor.h"
#include "Character/SnowBall_TPSCharHealthComponent.h"

bool USnowBallTPS_StateEffect::InitObject(AActor* Actor)
{
	myActor = Actor;

	ISnowBallTPS_IGameActor* myInterface = Cast<ISnowBallTPS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}

	return true;
}


void USnowBallTPS_StateEffect::DestroyObject()
{
	ISnowBallTPS_IGameActor* myInterface = Cast<ISnowBallTPS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}

	myActor = nullptr;

	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

bool USnowBallTPS_StateEffect_ExecuteOnce::InitObject(AActor* Actor)
{
	myActor = Actor;

	Super::InitObject(Actor);
	ExecuteOnce();

	return true;
}

void USnowBallTPS_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void USnowBallTPS_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		USnowBall_TPSHealthComponent* myHeathComp = Cast<USnowBall_TPSHealthComponent>(myActor->GetComponentByClass(USnowBall_TPSHealthComponent::StaticClass()));
		if (myHeathComp)
		{
			myHeathComp->ChangeHealthValue(Power);
		}
	}

	DestroyObject();
}

bool USnowBallTPS_StateEffect_ExecuteTimer::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &USnowBallTPS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &USnowBallTPS_StateEffect_ExecuteTimer::Execute, RateTime, true);

	if (ParticalEffect)
	{
		if (myActor)
		{
			ISnowBallTPS_IGameActor* myInterface = Cast<ISnowBallTPS_IGameActor>(myActor);

			if (myInterface)
			{
				FTransform EmitterTransform;
				if (myInterface->GetNameBoneForStateEffect(EmitterTransform) != "none")
				{
					FName NameBoneToAttach = myInterface->GetNameBoneForStateEffect(EmitterTransform);
					ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticalEffect, myActor->GetRootComponent(), NameBoneToAttach,
						EmitterTransform.GetLocation(), EmitterTransform.Rotator(), EAttachLocation::SnapToTarget, false);
				}
				else
				{
					ParticleEmitter = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticalEffect, EmitterTransform, false);
				}
			}
		}
	}
	return true;
}

void USnowBallTPS_StateEffect_ExecuteTimer::DestroyObject()
{
	if (ParticleEmitter)
	{
		ParticleEmitter->DestroyComponent();
	}
	ParticleEmitter = nullptr;
	Super::DestroyObject();
}

void USnowBallTPS_StateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		USnowBall_TPSHealthComponent* myHeathComp = Cast<USnowBall_TPSHealthComponent>(myActor->GetComponentByClass(USnowBall_TPSHealthComponent::StaticClass()));
		if (myHeathComp)
		{
			myHeathComp->ChangeHealthValue(Power);
		}
	}
}

