// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Interface/SnowBallTPS_IGameActor.h"
#include "SnowBall_TPS.h"

void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, TSubclassOf<USnowBallTPS_StateEffect> AddEffectClass, EPhysicalSurface SurfaceType)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{
		USnowBallTPS_StateEffect* myEffect = Cast<USnowBallTPS_StateEffect>(AddEffectClass->GetDefaultObject());
		if (myEffect)
		{
			bool bIsCanAdd = false;
			int8 i = 0;
			while (i < myEffect->PossibleInteractSurface.Num() && !bIsCanAdd)
			{
				if (myEffect->PossibleInteractSurface[i] == SurfaceType)
				{
					bIsCanAdd = true;
					USnowBallTPS_StateEffect* NewEffect = NewObject<USnowBallTPS_StateEffect>(TakeEffectActor, AddEffectClass);
					if (NewEffect)
					{
						ISnowBallTPS_IGameActor* myInterface = Cast<ISnowBallTPS_IGameActor>(TakeEffectActor);
							if (myInterface)
							{
								if (myInterface->CanAddStateEffect(AddEffectClass))
								{
									NewEffect->InitObject(TakeEffectActor);
								}
							}
					}
				}
				i++;
			}
		}
	}
}
