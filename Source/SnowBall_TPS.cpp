// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnowBall_TPS.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SnowBall_TPS, "SnowBall_TPS" );

DEFINE_LOG_CATEGORY(LogSnowBall_TPS)
 